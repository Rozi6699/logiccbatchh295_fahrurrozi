create database DB_HR

create table tb_karyawan (
ID int primary key identity (1,1) not null,
id_ int not null,
nip varchar (50) not null,
nama_depan varchar (50) not null,
nama_belakang varchar (50) not null,
jenis_kelamin varchar (50) not null,
agama varchar (50) not null,
tempat_lahir varchar (50) not null,
tgl_lahir date not null,
alamat varchar (100) not null,
pendidikan_terakhir varchar (50) not null,
tgl_masuk date not null)

alter table tb_karyawan drop column id_

create table tb_divisi (
ID int primary key identity (1,1) not null,
kd_divisi varchar (50) not null,
nama_divisi varchar (50) not null)

create table tb_jabatan (
ID int primary key identity (1,1) not null,
kd_jabatan varchar (50) not null,
nama_jabatan varchar (50) not null,
gaji_pokok int not null,
tunjangan_jabatan int not null)

create table tb_pekerjaan (
ID int primary key identity (1,1) not null,
nip varchar (50) not null,
kode_jabatan varchar (50) not null,
kode_divisi varchar (50) not null,
tunjangan_kinerja int not null,
kota_penempatan varchar (50) not null)

insert into tb_karyawan (id_, nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk) values
(1,'001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
(3,'003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
(2,'002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')
drop table tb_karyawan

insert into tb_divisi(kd_divisi, nama_divisi) values
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

insert into tb_jabatan (kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan) values
('MGR','Manager',5500000,1500000),
('OB','Office Boy',1900000,200000),
('ST','Staff',3000000,750000),
('WMGR','Wakil Manager',4000000,1200000)
drop table tb_jabatan

insert into tb_pekerjaan (nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan) values
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Sukabumi')
	select * from tb_karyawan
	select * from tb_divisi
	select * from tb_jabatan
	select * from tb_pekerjaan

--1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangann pekerjaannya dibawah 5juta
select nama_depan +'  '+  nama_belakang as nama_lengkap, jab.nama_jabatan, jab.tunjangan_jabatan + jab.gaji_pokok as total_tunjangan 
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip 
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
where jab.tunjangan_jabatan + jab.gaji_pokok < 5000000 

--2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi
create view vw_gaji as
select kar.nip, nama_depan +'  '+  nama_belakang as nama_lengkap, 
jab.nama_jabatan, div.nama_divisi, 
jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja as total_gaji,
(jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja) * 0.05 as pajak,
jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja - (jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja) * 0.05 as gaji_bersih 
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
join tb_divisi as div on pek.kode_divisi = div.kd_divisi
where kar.jenis_kelamin = 'Pria' and pek.kota_penempatan != 'Sukabumi'

select * from vw_gaji

--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja * 7)
select kar.nip, kar.nama_depan +'  '+ kar.nama_belakang as nama_lengkap,
jab.nama_jabatan, div.nama_divisi,
(((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja)*7)*0.25) as bonus
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
join tb_divisi as div on pek.kode_divisi = div.kd_divisi

--4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
select kar.nip, kar.nama_depan +'  '+ kar.nama_belakang as nama_lengkap, jab.nama_jabatan,
jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja as total_gaji,
(jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja) * 0.05 as infak 
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
join tb_divisi as div on pek.kode_divisi = div.kd_divisi
where jab.kd_jabatan = 'MGR'

--5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
select kar.nip, kar.nama_depan +'  '+ kar.nama_belakang as nama_lengkap, jab.nama_jabatan, kar.pendidikan_terakhir, 20000000 as tunjangan_terakhir,
jab.gaji_pokok+jab.tunjangan_jabatan+2000000 as total_gaji
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
join tb_divisi as div on pek.kode_divisi = div.kd_divisi
where kar.pendidikan_terakhir like '%S1%'
order by kar.nip asc

--6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
select kar.nip, kar.nama_depan +'  '+ kar.nama_belakang as nama_lengkap, jab.nama_jabatan, div.nama_divisi,
case 
when jab.kd_jabatan = 'MGR' then (((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja)*7)*0.25)
when jab.kd_jabatan = 'ST' then (((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja)*5)*0.25)
else (((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja)*2)*0.25)
end bonus
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
join tb_divisi as div on pek.kode_divisi = div.kd_divisi

--7. Buatlah kolom nip pada table karyawan sebagai kolom unique
alter table tb_karyawan
add constraint uc_karyawan unique (nip)

--8. buatlah kolom nip pada table karyawan sebagai index
create index index_nip
on tb_karyawan (nip)

--9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
select  nama_depan +'  '+ upper(nama_belakang) as nama_lengkap from tb_karyawan
where nama_belakang like 'W%'

--10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas  8 thn tahun
select kar.nama_depan +'  '+ kar.nama_belakang as nama_lengkap,kar.tgl_masuk, jab.nama_jabatan, div.nama_divisi,
case 
when datediff(year, kar.tgl_masuk, getdate()) > 8 
then ((jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja)*0.1)
else (jab.gaji_pokok+jab.tunjangan_jabatan+pek.tunjangan_kinerja)
end bonus,
 
datediff(year, kar.tgl_masuk, getdate()) as lama_bekerja 
from tb_karyawan as kar
join tb_pekerjaan as pek on kar.nip = pek.nip
join tb_jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
join tb_divisi as div on pek.kode_divisi = div.kd_divisi
where datediff(year, kar.tgl_masuk, getdate()) >= 8 









