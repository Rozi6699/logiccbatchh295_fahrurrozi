create database DBPenerbit

create table tblPengarang(
ID int primary key identity (1,1) not null,
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Alamat varchar (80) not null,
Kota varchar (15) not null,
Kelamin varchar (1) not null)

insert into tblPengarang (Kd_Pengarang, Nama, Alamat, Kota, Kelamin) values
('P0001','Ashadi','jl.Beo 25','Yogya','P'),
('P0002','Rian','jl.Solo 123','Yogya','P'),
('P0003','Suwadi','jl.Semangka 13','Bandung','P'),
('P0004','Siti','jl. Durian 15','Solo','W'),
('P0005','Amir','jl.Gajah 33','Kudus','P'),
('P0006','Suparman','jl.Harimau 25','Jakarta','P'),
('P0007','Jaja','jl.Singa 7','Bandung','P'),
('P0008','Saman','jl.Naga','Yogya','P'),
('P0009','Anwar','jl.Tidar 6A','Magelang','P'),
('P0010','Fatmawati','jl.Renjana 4','Bogor','W')

create table tblGaji(
ID int primary key identity (1,1) not null,
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Gaji Decimal (18,4) not null)

insert into tblGaji (Kd_Pengarang, Nama, Gaji) values
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P003','Suwadi',1000000),
('P0010','Fatmawati',600000),
('P0008','Saman',750000)

update tblGaji set Kd_Pengarang = 'P0003' where id = 4
update tblPengarang set Kota = 'Yogya' where Nama = 'Saman'

select *from tblPengarang
select * from tblGaji

select count (Kd_Pengarang) as jml_Pengarang from tblPengarang --1

select count(Kd_Pengarang) as jml_Pengarang, Kelamin --2
from tblPengarang  
group by Kelamin

select count(Kota) as jml_Kota, Kota --3
from tblPengarang  
group by Kota

select count(Kota) as jml_Kota, Kota --4
from tblPengarang  
group by Kota
having count(Kota) > 1

select kd_Pengarang from tblPengarang Where Kd_Pengarang = 'P0001' OR Kd_Pengarang = 'P0010' --5

select MIN(Gaji) as Gaji_Terkecil, MAX(Gaji) as Gaji_terendah from tblGaji --6

select Gaji --7
from tblGaji where gaji > 600000

select SUM(Gaji) from tblGaji --8

select c.Kota, sum(k.Gaji) from tblPengarang as c
join tblgaji as k
on  c.Kd_Pengarang = k.Kd_Pengarang
group by c.Kota

---9

select * from tblPengarang where ID >= 1 and ID <= 6 --10

select * from tblPengarang where Kota LIKE'Y%' or Kota Like 'S%'or Kota Like 'M%' --11

select * from tblPengarang where Kota not like 'Yogya%' --12

select * from tblPengarang where Nama like 'A%'  --13
select * from tblPengarang where Nama like '%I'
select * from tblPengarang where Nama like '__A%'
select * from tblPengarang where Nama not like '%n'

select * from tblPengarang --14
join tblGaji 
on tblPengarang.Kd_Pengarang = tblGaji.Kd_Pengarang

select kota as Kota, Gaji as Gaji from tblPengarang as c
right join tblgaji as k
on  c.Kd_Pengarang = k.Kd_Pengarang
where Gaji < 1000000 ---15

alter table tblPengarang alter column Kelamin varchar(10) --16

alter table tblPengarang add Gelar varchar(12) --17

update tblPengarang set Alamat = 'jl. Cendrawasih 65' , Kota = 'Yogya' where id = 2 --18

create view vwPengarang as --19
select Kd_Pengarang, Nama, Kota from tblPengarang 
select * from vwPengarang


select * from tblPengarang where Nama Like 'A%'
select * from tblPengarang where Nama Like '%i'
select * from tblPengarang where Nama Like '__a%'
select * from tblPengarang where Nama not like '%n'

select paja.*, gaja.Gaji
from tblPengarang as paja
join tblGaji as gaja  
on paja.Kd_Pengarang = gaja.Kd_Pengarang