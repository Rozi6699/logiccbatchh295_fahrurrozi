using System;

namespace LogicDay1
{
    internal class Program
    {
        static void Main(String[] args)
        {
            Tugas1();
            Tugas2();
            Console.ReadKey();
        }
        static void Tugas1()
        {
            int nilai = 75;
            Console.Write("Masukkan Nilai Kamu  = ");
            nilai = int.Parse(Console.ReadLine());
            if (nilai >= 80)
            {
                Console.WriteLine("Kamu Mendapatkan Grade A ");
            }
            else if (nilai >= 60)
            {
                Console.WriteLine("Kamu Mendapatkan Grade B");
            }
            else
            {
                Console.WriteLine("Kamu Mendapatkan Grade C");
            }
        }
            static void Tugas2()
            {
                int x = 0;
                Console.Write("Masukkan Nilai  = ");
                x = int.Parse(Console.ReadLine());
                if (x % 2 == 0)
                {
                    Console.WriteLine($"Angka {x} adalah Genap");
                }
                else
                {
                    Console.WriteLine($"Angka {x} adalah Ganjil");
                }
            }
        }
    }