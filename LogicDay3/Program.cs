﻿using System;

namespace LogicDay3
{
    internal class Program
    {
        static void Main(String[] args)
        {
            Console.Write("Pilih Soal Kamu = ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "Soal1":
                    Soal1();
                    break;
                case "Soal2":
                    Soal2();
                    break;
                case "Soal3":
                    Soal3();
                    break;
                case "Soal4":
                    Soal4();
                    break;
                case "Soal5":
                    Soal5();
                    break;
                case "Soal6":
                    Soal6();
                    break;
                case "Soal7":
                    Soal7();
                    break;
                case "Soal8":
                    Soal8();
                    break;
                case "Soal9":
                    Soal9();
                    break;
                case "Soal10":
                    Soal10();
                    break;
            }
        }
        static void Soal1()
        {
            int n = 7;
            int angka = 1;
            for (int i = 0; i < n; i++)
            {
                Console.Write(angka + " ");
                angka += 2;

            }
        }
        static void Soal2()
        {
            int n = 7;
            int z = 2;
            for (int i = 0; i < n; i++)
            {
                Console.Write(z + " ");
                z += 2;
            }
        }
        
        static void Soal3()
        {
            int n = 7;
            int angka = 1;
            for (int i = 0; i < n; i++)
            {
                Console.Write(angka + " ");
                angka += 3;
            }
        }
        static void Soal4()
        {
            int n = 7;
            int r = 1;
            for (int i = 1; i < n; i++)
            {
                Console.Write(r + " ");
                r += 4;
            }
        }
        static void Soal5()
        {
            int n = 7;
            int f = 1;

            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write($"* ");
                }
                else
                {
                    Console.Write($" {f} ");
                    f += 4;
                }
                
            }
        }
        static void Soal6()
        {
            int n = 7;
            int f = 1;

            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write($"* ");
                    f += 4;
                }
                else
                {
                    Console.Write($" {f} ");
                    f += 4;
                }
            }
        }
        static void Soal7()
        {
            int n = 8;
            int f = 2;

            for (int i = 2; i <= n; i++)
            {
                Console.Write(f + " ");
                f *= 2;
            }
        }
        static void Soal8()
        {
            int n = 7;
            int f = 1;

            for (int i = 1; i <= n; i++)
            {
                f *= 3;
                Console.Write(f + " ");
                
            }
        }
        static void Soal9()
        {
            int n = 7;
            int f = 1;

            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    f *= 4;
                    Console.Write($" {f} ");

                }
            }
        }
        static void Soal10()
        {
            int n = 7;
            int f = 3;

            for (int i = 1; i <= n; i++)
            {
                if (i % 4 == 0)
                {
                     
                    Console.Write("XXX ");
                    
                }
                else
                {
                    
                    Console.Write($" {f} ");
                    
                }
                f *= 3;
            }
        }
    }
}

