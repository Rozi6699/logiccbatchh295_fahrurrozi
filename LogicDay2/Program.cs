﻿using System;

namespace LogicDay1
{
    internal class Program
    {
        static void Main(String[] args)
        {
            Console.Write("Pilih Soal (Soal1/Soal2/Soal3/Soal4/Soal5/Soal6/Soal7/Soal8) = ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "Soal1":
                    Soal1();
                    break;
                case "Soal2":
                    Soal2();
                    break;
                case "Soal3":
                    Soal3();
                    break;
                case "Soal4":
                    Soal4();
                    break;
                case "Soal5":
                    Soal5();
                    break;
                case "Soal6":
                    Soal6();
                    break;
                case "Soal7":
                    Soal7();
                    break;
                case "Soal8":
                    Soal8();
                    break;
            }
        }
        static void Soal1()
        {
            int nilai = 0;
            Console.Write("Masukkan Nilai  = ");
            nilai = int.Parse(Console.ReadLine());
            if (nilai >= 85)
            {
                Console.WriteLine("Nilai A");
            }
            else if (nilai >= 70)
            {
                Console.WriteLine("Nilai B");
            }
            else
            {
                Console.WriteLine("Nilai C");
            }
        }
        static void Soal2()
        {
            int harga;
            Console.Write("Masukkan Harga   = ");
            harga = int.Parse(Console.ReadLine());
            if (harga >= 10000 && harga <= 24999)
            {
                Console.WriteLine($"Pulsa   = {harga}");
                Console.WriteLine("Point    = 80");
            }
            else if (harga >= 25000 && harga <= 49999 )
            {
                Console.WriteLine($"Pulsa   = {harga}");
                Console.WriteLine("Point    = 200");
            }
            else if (harga >= 50000 && harga <= 99999)
            {
                Console.WriteLine($"Pulsa   = {harga}");
                Console.WriteLine("Point    = 400");
            }
            else
            {
                Console.WriteLine($"Pulsa   = {harga}");
                Console.WriteLine("Point    = 800");
            }


        }
        static void Soal3()
        {
            int belanja, jarak, diskon, ongkir;
            string promo;
            double totalBelanja, ifdiskon;

            Console.Write("Belanja =");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Jarak = ");
            jarak = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Promo = ");
            promo = Console.ReadLine();

            if (promo != "JKTOVO" || belanja < 30000)
            {

                totalBelanja = belanja + (jarak * 1000);
                Console.WriteLine($"Belanja = {belanja}");
                Console.WriteLine("Diskon Tidak Dapat");
                Console.WriteLine($"Ongkir = {jarak * 1000}");
                Console.WriteLine($"Total Belanja = {totalBelanja}");
            }
            else if (promo != "JKTOVO" && belanja >= 30000)
            {
                totalBelanja = belanja + (jarak * 1000);
                Console.WriteLine($"Belanja = {belanja}");
                Console.WriteLine("Diskon Tidak Dapat");
                Console.WriteLine($"Ongkir {jarak * 1000}");
                Console.WriteLine($"Total Belanja = {totalBelanja}");
            }
            else
            {
                ifdiskon = 0.4 * belanja;
                if (ifdiskon <= 30000) 
                {
                    totalBelanja = belanja + (jarak * 1000) - ifdiskon;
                    Console.WriteLine($"Belanja = {belanja}");
                    Console.WriteLine($"Diskon = {0.4 * belanja} ");
                    Console.WriteLine($"Ongkir {jarak * 1000}");
                    Console.WriteLine($"Total Belanja = {totalBelanja}");
                }
                else
                {
                    totalBelanja = belanja + (jarak * 1000) - 30000;
                    Console.WriteLine($"Belanja = {belanja}");
                    Console.WriteLine($"Diskon = 30000 ");
                    Console.WriteLine($"Ongkir {jarak * 1000}");
                    Console.WriteLine($"Total Belanja = {totalBelanja}");
                }

            }
        
    }

    static void Soal4()
    {
            int belanja, ongkosKirim, pilihVoucher, diskonOngkir, diskonBelanja, Total;

            Console.Write("1.Min Order 30rb Free Ongkir 5rb dan Potongan Harga Belanja 5rb.");
            Console.Write("2. Min Order 50rb Free Ongkir 10rb dan Potongan Harga Belanja 10rb.");
            Console.Write("3. Min Order 100rb Free Ongkir 20rb dan Potongan Harga Belanja 10rb.");
            

            Console.Write("Belanja       = ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Ongkos Kirim  = ");
            ongkosKirim = int.Parse(Console.ReadLine());
            Console.Write("Pilih Voucher =");
            pilihVoucher = int.Parse(Console.ReadLine());

            switch (pilihVoucher)
            {
                case 1:
                    if (belanja >= 30000)
                    {
                        diskonOngkir = 5000;
                        diskonBelanja = 5000;
                        Total = belanja + ongkosKirim - diskonOngkir - diskonBelanja;
                        Console.WriteLine($"Belanja         = {belanja}");
                        Console.WriteLine($"Ongkos Kirim    = {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   = {diskonOngkir}");
                        Console.WriteLine($"Diskon Belanja  = {diskonBelanja}");
                        Console.WriteLine($"Total Belanja   = {Total}");
        
                    }
                    break;
                case 2:
                    if (belanja >= 50000)
                    {
                        diskonOngkir = 10000;
                        diskonBelanja = 10000;
                        Total = belanja + ongkosKirim - diskonOngkir - diskonBelanja;
                        Console.WriteLine($"Belanja = {belanja}");
                        Console.WriteLine($"Ongkos Kirim    = {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   = {diskonOngkir}");
                        Console.WriteLine($"Diskon Belanja  = {diskonBelanja}");
                        Console.WriteLine($"Total Belanja   = {Total}");
                        
                    }
                    break;
                case 3:
                    if (belanja >= 100000)
                    {
                        diskonOngkir = 20000;
                        diskonBelanja = 10000;
                        Total = belanja + ongkosKirim - diskonOngkir - diskonBelanja;
                        Console.WriteLine($"Belanja         = {belanja}");
                        Console.WriteLine($"Ongkos Kirim    = {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   = {diskonOngkir}");
                        Console.WriteLine($"Diskon Belanja  = {diskonBelanja}");
                        Console.WriteLine($"Total Belanja   = {Total}");
                        
                    }
                    break;
            }


            }

        static void Soal5()
        {
            int boomer, genx, geny, genz, tahunLahir;
            string nama;

            Console.WriteLine("Masukkan Nama Anda       = ");
            nama = Console.ReadLine();
            Console.WriteLine("Tahun Berapa Anda Lahir  = ");
            tahunLahir = int.Parse(Console.ReadLine());

            if (tahunLahir >= 1944 && tahunLahir <= 1964)
            {
                Console.WriteLine($"Masukkan Nama Anda          = {nama}");
                Console.WriteLine($"Tahun Berapa Anda Lahir     = {tahunLahir}");
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Boomer");
            }

            else if (tahunLahir >= 1965 && tahunLahir <= 1979)
            {
                Console.WriteLine($"Masukkan Nama Anda          = {nama}");
                Console.WriteLine($"Tahun Berapa Anda Lahir     = {tahunLahir}");
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
            }

            else if (tahunLahir >= 1980 && tahunLahir <= 1994)
            {
                Console.WriteLine($"Masukkan Nama Anda          = {nama}");
                Console.WriteLine($"Tahun Berapa Anda Lahir     = {tahunLahir}");
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y (Millenials)");
            }

            else if (tahunLahir >= 1995 && tahunLahir <= 2015)
            {
                Console.WriteLine($"Masukkan Nama Anda          = {nama}");
                Console.WriteLine($"Tahun Berapa Anda Lahir     = {tahunLahir}");
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
            }
            else
            {
                Console.WriteLine("Maaf, Interval Umur Hanya Dari Tahun 1944 - 2015");
            }
        }
        static void Soal6()
        {
            string nama;
            double pajak, bpjs, gapok, tunjangan, gaji, banyakbulan, totalGaji;

            Console.WriteLine("Input        = ");
            Console.Write("Nama         = ");
            nama = Console.ReadLine();
            Console.Write("Tunjangan    = ");
            tunjangan = double.Parse(Console.ReadLine());
            Console.Write("Gapok        = ");
            gapok = double.Parse(Console.ReadLine());
            Console.Write("Banyak Bulan = ");
            banyakbulan = double.Parse(Console.ReadLine());

            if (gapok + tunjangan <= 5000000)
            {
                pajak = 0.05 * (gapok + tunjangan);
                bpjs = 0.03 * (gapok + tunjangan);
                gaji = (gapok + tunjangan) - (pajak + bpjs);
                totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakbulan;

                Console.WriteLine($"Pajak                   = {pajak}");
                Console.WriteLine($"BPJS                    = {bpjs}");
                Console.WriteLine($"Gaji/Bln                = {gaji}");
                Console.WriteLine($"Total Gaji/Banyak Bulan = {totalGaji}");

            }
            else if (gapok + tunjangan >= 5000000 && gapok + tunjangan <= 10000000) 
            {
                pajak = 0.1 * (gapok + tunjangan);
                bpjs = 0.03 * (gapok + tunjangan);
                gaji = (gapok + tunjangan) - (pajak + bpjs);
                totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakbulan;

                Console.WriteLine($"Pajak                   = {pajak}");
                Console.WriteLine($"BPJS                    = {bpjs}");
                Console.WriteLine($"Gaji/Bln                = {gaji}");
                Console.WriteLine($"Total Gaji/Banyak Bulan = {totalGaji}");
            }
        
               else
            {
                pajak = 0.15 * (gapok + tunjangan);
                bpjs = 0.03 * (gapok + tunjangan);
                gaji = (gapok + tunjangan) - (pajak + bpjs);
                totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakbulan;

                Console.WriteLine($"Pajak                   = {pajak}");
                Console.WriteLine($"BPJS                    = {bpjs}");
                Console.WriteLine($"Gaji/Bln                = {gaji}");
                Console.WriteLine($"Total Gaji/Banyak Bulan = {totalGaji}");
            }

        }
        static void Soal7()
        {
            double beratBadan, tinggiBadan, bmi, Tinggi;

            Console.Write("Berat Badan         = ");
            beratBadan = double.Parse(Console.ReadLine());
            Console.Write("Tinggi Badan        = ");
            tinggiBadan = double.Parse(Console.ReadLine());

            Tinggi = 0.01 * tinggiBadan;
            bmi = beratBadan / (Tinggi * Tinggi);

            if (bmi < 18.5)

            {
                Console.WriteLine($"Masukkan Berat Badan Anda       = {beratBadan}");
                Console.WriteLine($"Masukkan Tinggi Badan Anda      = {tinggiBadan}");
                Console.WriteLine($"Nilai BMI anda adalah           = {bmi}");
                Console.WriteLine("Anda Termasuk Terlalu Kurus");
            }
            else if (bmi > 18.5 && bmi < 25)
            {
                Console.WriteLine($"Masukkan Berat Badan Anda       = {beratBadan}");
                Console.WriteLine($"Masukkan Tinggi Badan Anda      = {tinggiBadan}");
                Console.WriteLine($"Nilai BMI anda adalah           = {bmi}");
                Console.WriteLine("Anda Termasuk Berbadan Langsing/Sehat");
            }
            else
            {
                Console.WriteLine($"Masukkan Berat Badan Anda       = {beratBadan}");
                Console.WriteLine($"Masukkan Tinggi Badan Anda      = {tinggiBadan}");
                Console.WriteLine($"Nilai BMI anda adalah           = {bmi}");
                Console.WriteLine("Anda Termasuk Gemuk");
            }
        }
        static void Soal8()
        {
            int mtk, fisika, kimia;
            double rata;

            Console.Write("Masukkan Nilai MTK          = ");
            mtk = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai fisika       = ");
            fisika = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai kimia        = ");
            kimia = int.Parse(Console.ReadLine());

            rata = ((mtk + fisika + kimia) / 3);

            if (rata > 50)
            {
                Console.WriteLine($"Masukkan Nilai MTK          = {mtk}");
                Console.WriteLine($"Masukkan Nilai fisika       = {fisika}");
                Console.WriteLine($"Masukkan Nilai kimia        = {kimia}");
                Console.WriteLine($"Nilai Rata - Rata           = {rata}");
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu Berhasil");
                Console.WriteLine("Kamu Hebat");
            }
            else
            {
                Console.WriteLine($"Masukkan Nilai MTK          = {mtk}");
                Console.WriteLine($"Masukkan Nilai fisika       = {fisika}");
                Console.WriteLine($"Masukkan Nilai kimia        = {kimia}");
                Console.WriteLine($"Nilai Rata - Rata           = {rata}");
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu Gagal");
            }
        }


        }

            }



       
    


    

