create database DB_Entertainer

create table artis (
ID int primary key identity (1,1) not null,
kd_artis varchar (100) not null,
nm_artis varchar (100) not null,
jk varchar (100) not null,
bayaran decimal (18,4) not null,
award int not null,
negara varchar (100) not null )

insert into artis (kd_artis, nm_artis, jk, bayaran, award, negara) values
('AD01','ROBERT DOWNEY JR','PRIA','3000000000','2','AS'),
('AD02','ANGELINA JOLIE','WANITA','700000000','1','AS'),
('AD03','JACKIE CHAN','PRIA','200000000','7','HK'),
('AD04','JOE TASLIM','PRIA','350000000','1','ID'),
('AD05','CHELSEA ISLAN','WANITA','300000000','0','ID')

create table film (
ID int primary key identity (1,1) not null,
kd_film varchar (10) not null,
nm_film varchar (55) not null,
genre varchar (55) not null,
artis varchar (55) not null,
produser varchar (55) not null,
pendapatan decimal (18,4) not null,
nominasi int not null)

insert into film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi) values
('F001','IRON MAN','G001','AD01','PD01',2000000000,'3'),
('F002','IRON MAN 2','G001','AD01','PD01',1800000000,'2'),
('F003','IRON MAN 3','G001','AD01','PD01',1200000000,'0'),
('F004','AVENGER:CIVIL WAR','G001','AD01','PD01',2000000000,'1'),
('F005','SPIDERMAN HOME COMING','G001','AD01','PD01',13000000000,'0'),
('F006','THE RAID','G001','AD04','PD03',800000000,'5'),
('F007','FAST & FURIOUS','G001','AD04','PD05',830000000,'2'),
('F008','HABIBIE DAN AINUN','G004','AD05','PD03',670000000,'4'),
('F009','POLICE STORY','G001','AD03','PD02',700000000,'3'),
('F010','POLICE STORY 2','G001','AD03','PD02',710000000,'1'),
('F011','POLICE STORY 3','G001','AD03','PD02',615000000,'0'),
('F012','RUSH HOUR','G003','AD03','PD05',695000000,'2'),
('F013','KUNGFU PANDA','G003','AD03','PD05',923000000,'5')

update film set pendapatan= '1300000000' where kd_film = 'F005'
drop table artis

create table produser (
ID int primary key identity (1,1) not null,
kd_produser varchar (50) not null,
nm_produser varchar (50) not null,
international varchar (50) not null)

insert into produser (kd_produser, nm_produser, international) values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

create table negara (
ID int primary key identity (1,1) not null,
kd_negara varchar(100) not null,
nm_negara varchar (100) not null)

insert into negara (kd_negara, nm_negara) values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

create table genre (
ID int primary key identity (1,1) not null,
kd_genre varchar (50) not null,
nm_genre varchar (50) not null)

insert into genre (kd_genre, nm_genre) values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

select * from artis
select * from film
select * from produser
select * from negara
select * from genre

select  prod.nm_produser, SUM(fil.pendapatan) as pendapatan from  film as fil --1
join produser as prod 
on fil.produser = prod.kd_produser
group by prod.nm_produser 
having prod.nm_produser='MARVEL'

select nm_film,nominasi from film where nominasi=0 --2

select nm_film, pendapatan from film where
pendapatan = (select max(pendapatan) from film)
select top 2 nm_film, pendapatan from film --3  

select nm_film from film where nm_film like 'P%' --4

select nm_film from film where nm_film like '%Y' --5

select nm_film from film where nm_film like '%d%' --6

select fil.nm_film, art.nm_artis --7
from film as fil
join artis as art 
on fil.artis = art.kd_artis

select fil.nm_film, art.negara --8
from film as fil
join artis as art 
on fil.artis = art.kd_artis
where art.negara ='HK'

select fil.nm_film, neg.nm_negara --9
from film as fil
join artis as art on fil.artis = art.kd_artis
join negara as neg on art.negara = neg.kd_negara
where neg.nm_negara not like '%o%'

select nm_artis from film as fil --10
right join artis as art on fil.artis = art.kd_artis
where fil.artis is null

select art.nm_artis, gen.nm_genre --11
from film as fil 
join genre as gen on fil.genre = gen.kd_genre
join artis as art on fil.artis = art.kd_artis
where gen.nm_genre= 'DRAMA'

select art.nm_artis, gen.nm_genre --12
from film as fil 
join genre as gen on fil.genre = gen.kd_genre
join artis as art on fil.artis = art.kd_artis
group by art.nm_artis, gen.nm_genre
having gen.nm_genre = 'ACTION'

select neg.kd_negara, neg.nm_negara, count(fil.nm_film) as jumlah_film --13
from film as fil
join artis as art on fil.artis = art.kd_artis
right join negara as neg on art.negara = neg.kd_negara 
group by neg.kd_negara, neg.nm_negara

select nm_film  --14
from film as fil
join produser as prod on fil.produser = prod.kd_produser
where prod.international = 'YA'

select nm_produser, count(fil.nm_film) as jumlah_film --15                                                                                   
from film as fil
right join produser as prod on fil.produser = prod.kd_produser
group by nm_produser

create vwpembayaran
select *, --metode case
case
when bayaran >= 500000000 Then bayaran*0.2
when bayaran < 500000000 and  bayaran > 200000000 then bayaran *0.1
else 0
end as pajak,
case 
when bayaran  >= 500000000 then bayaran - (bayaran*0.2)
when bayaran < 500000000 and bayaran > 200000000 then bayaran - (bayaran*0.1)
else bayaran 
end as Total_Bayaran
from artis

select sum(Total_Bayaran from view vwpembayaran

select kd_artis + nm_artis as kd_nama from artis -- + ga bisa sama integer
select concat(kd_artis,' ', nm_artis) as kd_nama from artis -- concat bisa sama integer

