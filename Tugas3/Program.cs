﻿using System;
using System.Linq;

namespace Tugas3
{
    internal class Program
    {
        static void Main(String[] args)
        {

            {
                Console.Write("Pilih Soal Kamu = ");
                string pilih = Console.ReadLine();

                switch (pilih)
                {
                    case "Soal1":
                        Soal1();
                        break;
                    case "Soal2":
                        Soal2();
                        break;
                    case "Soal3":
                        Soal3();
                        break;
                    case "Soal4":
                        Soal4();
                        break;

                }
            }
        }
        static void Soal1()
        {
            string masuk;
            Console.Write("Masukkan Kata = ");
            masuk = Console.ReadLine();
            String[] katakata = masuk.Split(' ');
            foreach (string kata in katakata)
                for (int i = 0; i < kata.Length; i++)
                {

                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write($"{kata[i]}");
                    }
                    else
                        Console.Write($"*");
                }

        }
        static void Soal2()
        {
            string masuk;
            Console.Write("Masukkan Kata = ");
            masuk = Console.ReadLine();
            String[] katakata = masuk.Split(' ');

            foreach (string kata in katakata)
                for (int i = 0; i < kata.Length; i++)
                {

                    if (i == 0 || i == kata.Length - 1)
                    {
                        Console.Write($"*");

                    }
                    else
                        Console.Write($"{kata[i]}");

                }

        }
        static void Soal3()
        {
            string input, hasil;
            Console.Write("Input = ");
            input = Console.ReadLine();
            hasil = string.Empty;
            for (int i = input.Length - 1; i >= 0; i--)
            {
                hasil += input[i];

            }
            if (input == hasil)
            {
                Console.Write("YES");
            }
            else
            {
                Console.Write("NO");

            }
        }
        static void Soal4()
        {
            Console.Write("Masukkan Uang Yang Dimiliki =");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Harga Celana = ");
            String celana = Console.ReadLine();
            String[] arrayCelana = celana.Split(",");
            int[] Celana = Array.ConvertAll(arrayCelana, int.Parse);

            Console.Write("Masukkan Harga Baju = ");
            String baju = Console.ReadLine();
            String[] arrayBaju = baju.Split(",");
            int[] Baju = Array.ConvertAll(arrayBaju, int.Parse);


            int[] jumlah = new int[Baju.Length];
            int max = 0;

            for (int i = 0; i < Baju.Length; i++)
            {
                int temp = (Baju[i] + Celana[i]);

                if (temp <= input)
                {
                    jumlah[i] = temp;
                    //if (temp > max)
                    //{
                    //    max = temp;
                    //}
                }
                


            }
            Console.Write($"Kamu dapat membeli baju dan celana dengan harga {jumlah.Max()}");

        }
    }
}

       