﻿using System;

namespace LogicDay1
{
    internal class Program
    {
        static void Main(String[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.Write("Pilih Soal Kamu = ");
                string pilih = Console.ReadLine();

                switch (pilih)
                {
                    case "Soal1":
                        Soal1();
                        break;
                    case "Soal2":
                        Soal2();
                        break;
                    case "Soal3":
                        Soal3();
                        break;
                    case "Soal4":
                        Soal4();
                        break;
                    case "Soal5":
                        Soal5();
                        break;
                    case "Soal6":
                        Soal6();
                        break;
                }
                Console.WriteLine();
                Console.Write("Ulangi Proses ? (y/n)  ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }
            }
        }
        static void Soal1()
        {
            double x, y;
            Console.Write("Masukkan Nilai  = ");
            x = int.Parse(Console.ReadLine());
            for (int i = 2; i <= 5; i++)
            {
                if (x % i == 0)
                {
                    y = x / i;
                    Console.WriteLine($"{x}/{i}={y}");
                    x = y;
                    if (x % i == 0)
                    {
                        continue;
                    }
                }

            }

        }
        static void Soal2()
        {
            int f, g = 0;

            for (f = 0; f < 5; f++)
            {
                for (g = 0; g < 5; g++)
                {
                    Console.Write("({0}, {1})", f, g);
                }
                Console.Write("\n");
            }
        }
        static void Soal3()
        {
            int n = 7;
            int f = 3;

            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 0)
                {

                    Console.Write("* ");

                }
                else
                {
                    Console.Write($" {f} ");

                }
                f *= 3;

            }
        }
        static void Soal4()
        {
            int n = 7;
            int f = 5;

            for ( int i = 1; i <= n; i++)
            {
                if (i % 2 == 1)
                {
                    
                    Console.Write($" -{f} ");
                    f += 5;
                }
                else
                {

                    Console.Write($"{f} ");
                    f += 5;
                }
                
            }

        }

        static void Soal5()
        {
            int no1 = 1, no2 = 1, no3 = 1, i, nomer;
            Console.Write("Input Angka = ");
            nomer = int.Parse(Console.ReadLine());
            Console.Write(no1 + " " + no2 + " ");
            for (i = 2; i < nomer; ++i)
            {
                no3 = no1 + no2;
                Console.Write(no3 + " ");
                no1 = no2;
                no2 = no3;

            }
        }
        static void Soal6()
        {
            int no1 = 1, no2 = 1, no3 = 1, no4, i, nomer;
            Console.Write("Input Angka = ");
            nomer = int.Parse(Console.ReadLine());
            Console.Write(no1 + " " + no2 + " " + no3 + " ");
            for (i = 3; i < nomer; ++i)
            {
                no4 = no1 + no2 + no3;
                Console.Write(no4 + " ");
                no1 = no2;
                no2 = no3;
                no3 = no4;
            }
        }
    }
}

