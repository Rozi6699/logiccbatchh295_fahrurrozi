﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace LogicDay6
{
    internal class Program
    {
        static void Main(String[] args)
        {

            {
                Console.Write("Pilih Soal Kamu = ");
                string pilih = Console.ReadLine();

                switch (pilih)
                {
                    case "Soal1":
                        Soal1();
                        break;
                    case "Soal2":
                        Soal2();
                        break;
                    case "Soal3":
                        Soal3();
                        break;
                    case "Soal4":
                        Soal4();
                        break;
                    case "Soal5":
                        Soal5();
                        break;
                }
            }
        }
        static void Soal1()
        {

            string huruf;

            Console.Write("Huruf : ");
            huruf = Console.ReadLine();

            int lembah = 0;
            int batas = 0;

            for (int i = 0; i < huruf.Length; i++)
            {

                if (huruf[i] == 'D')
                {
                    batas--;
                }
                else if (huruf[i] == 'U')
                {
                    batas++;
                }
                if (huruf[i] == 'D' && batas == -1)
                {
                    lembah++;
                }

            }
            Console.Write($"{lembah}");
        }

        static void Soal2()
        {
            {

                Console.Write("Masukan Kalimat = ");
                string kalimat = Console.ReadLine().ToLower();
                Console.Write("Masukan Jumlah Rotate = ");
                int rotate = int.Parse(Console.ReadLine());


                string alphabet = "abcdefghijklmnopqrstuvwxyz";
                string alphabet1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string Alphabetbaru = "";
                string Alphabetbaruup = "";
                string newkata = "";
                string kata = alphabet;
                string kataBaru = alphabet1;

                //untuk menglooping
                for (int i = 0; i < rotate; i++)
                {
                    Alphabetbaru = kata.Substring(1, kata.Length - 1) + kata[0];
                    kata = Alphabetbaru;
                    Alphabetbaruup = kataBaru.Substring(1, kataBaru.Length - 1) + kataBaru[0];
                    kataBaru = Alphabetbaruup;
                }
                //mencari huruf didalam kalimat yg di input
                foreach (char huruf in kalimat)
                {
                    int indeks = alphabet.IndexOf(huruf); //untuk mencari indeks dari kalimat yg yg diinput di dalam alphabet 
                    int indeksup = alphabet1.IndexOf(huruf); //untuk mencari indeks dari kalimat yg yg diinput di dalam alphabetup

                    if (indeks != -1)
                    {
                        newkata += Alphabetbaru[indeks];
                    }
                    else if (indeksup != -1)
                    {
                        newkata += Alphabetbaruup[indeksup];
                    }
                    else
                    {
                        newkata += huruf;
                    }
                }
                Console.WriteLine($"Alphabet = {alphabet}");
                Console.WriteLine($"Alphabet Baru = {Alphabetbaruup}");
                Console.WriteLine($"Kalimat Baru = {newkata}");
            }
        }
        static void Soal3()
        {
            Console.Write("Input tinggi : ");
            int[] tinggi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            Console.Write("Input Kalimat : ");
            string kalimat = Console.ReadLine();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            List<int> simpan = new List<int>();
            for (int i = 0; i < kalimat.Length; i++)
            {
                char huruf = kalimat[i];
                int index = alfabet.IndexOf(huruf);

                simpan.Add(tinggi[index]);
                Console.WriteLine($"index alfabet {huruf} = {index} maka ke index {index} di elemen tertinggi {(tinggi[index])}");
            }
            int len = kalimat.Length;
            int max = simpan.Max();

            int hasil = len * max;

            Console.WriteLine($"{len}*{max}={hasil}");
        }
    


        static void Soal4()
        {
            {
                string vokal = "aiueoAIUEO";
                string konsonan = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

                Console.Write("Input = ");
                string input = Console.ReadLine().ToLower();
                int x;
                int nilai = 0;

                Console.Write("\nHuruf Vokal = ");
                string[] arrayInput1 = new string[input.Length];
                for (x = 0; x < input.Length; x++)
                {
                    nilai = vokal.IndexOf(input[x]);
                    if (nilai > -1)
                    {
                        arrayInput1[x] = input[x].ToString();
                    }
                }
                Array.Sort(arrayInput1);
                foreach (string hasil in arrayInput1)
                {
                    Console.Write(hasil);
                }
                Console.WriteLine();

                //untuk mengurutkan dari abjad awal ke akhir
                Console.Write("\nHuruf Konsonan = ");
                string[] arrayInput2 = new string[input.Length];
                for (x = 0; x < input.Length; x++)
                {
                    nilai = konsonan.IndexOf(input[x]);
                    if (nilai > -1)
                    {
                        arrayInput2[x] = input[x].ToString();
                    }
                }
                //untuk mengurutkan dari abjad awal ke akhir
                Array.Sort(arrayInput2);
                foreach (string hasil in arrayInput2)
                {
                    Console.Write(hasil);
                }
                Console.WriteLine();
            }
        }
        static void Soal5 ()
        {
            Console.Write("Password = ");
            string password = Console.ReadLine();

            string HurufBesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string HurufKecil = "abcdefghijklmnopqrstuvwxyz";
            string Angka = "1234567890";
            string Simbol = "~!@#$%^&*()_+-={}[]|<>?/:;";
            int x, nilai;
            int hasil1 = 0;
            int hasil2 = 0;
            int hasil3 = 0;
            int hasil4 = 0;

            for (x = 0; x < password.Length; x++)
            {
                nilai = HurufBesar.IndexOf(password[x]);
                if (nilai > -1)
                {
                    hasil1++;

                }
            
                Console.Write("Pass weak kurang angka dan symbol");
            }

            for (x = 0; x < password.Length; x++)
            {
                nilai = HurufKecil.IndexOf(password[x]);
                if (nilai > -1)
                {

                    hasil2++;

                }
            }
            Console.Write("Pass weak kurang huruf besar,angka dan symbol");
            for (x = 0; x < password.Length; x++)
            {
                nilai = Angka.IndexOf(password[x]);
                if (nilai > -1)
                {
                    hasil3++;

                }
            }
            Console.Write("Pass weak kurang huruf besar dan symbol");
            for (x = 0; x < password.Length; x++)
            {
                nilai = Simbol.IndexOf(password[x]);
                if (nilai > -1)
                {
                    hasil4++;

                }

            }
            Console.Write("Pass weak kurang huruf besar angka");

            if (password.Length < 6)
            {
                Console.Write("Password Weak Dan Kurang dari 6 digit dan kurang symbol");
            }
            else
            {
                if (hasil1 > 0 && hasil2 > 0 && hasil3 > 0 && hasil4 > 0)
                {
                    Console.WriteLine("Password Strong");
                }
                else
                {
                    Console.WriteLine("Password Weak");
                }
            }
        }
    }
}
