﻿using System;

namespace LogicDay4
{
    internal class Program
    {
        static void Main(String[] args)
        {
            Console.Write("Pilih Soal Kamu = ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "Soal1":
                    Soal1();
                    break;
                case "Soal2":
                    Soal2();
                    break;
                case "Soal3":
                    Soal3();
                    break;
                case "Soal4":
                    Soal4();
                    break;
                case "Soal5":
                    Soal5();
                    break;
              
                
            }
        }
        static void Soal1()
        {
            int input;
            int[] a = new int[7];

            Console.Write("Masukkan Input Array = ");
            input = int.Parse(Console.ReadLine());
            for (int b = 0; b < input; b++)
            {

                Console.Write("Masukkan data = ");
                a[b] = int.Parse(Console.ReadLine());
            }

            Array.Sort(a);
            Console.WriteLine("\nSorted List");
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
            }
        }
        static void Soal2()
        {
            Console.Write("Masukkan Nilai : ");
            int masuk = int.Parse(Console.ReadLine());
            for (int i = 1; i <= masuk; i++)
            {
                int f = 0;
                for (int j = 2; j <= i / 2; j++)
                {
                    if (i % j == 0)
                    {
                        f++;
                        break;
                    }
                }

                if (f == 0 && i != 1)
                {
                    Console.Write("{0} ", i);
                }
            }
            Console.ReadKey();
        }
        static void Soal3()
        {
            int p, d, m, s, total=0;

            Console.Write("Masukkan Nilai P = ");
            p = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai D = ");
            d = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai M = ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai S = ");
            s = int.Parse(Console.ReadLine());

            while (p>=m && s >=p)
            {

                s -= p;
                p -= d;
                

                if (p < m)
                {

                    p = m;

                }
                total++;
                
            }
            
            Console.Write($"{total}  Video Game");
        }
        static void Soal4()
        {
            int f;
            Console.Write("Masukan Angka = ");
            f = int.Parse(Console.ReadLine());
            for (int i = 1; i <= f; i++)
            {
                for (int j = i; j <= f; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");
            }
        }
        static void Soal5()
        {
            
                int n = 0;
                int jumlah = 0;
                Console.Write("Masukkan Sinyal : ");
                string masukkan = Console.ReadLine();

                if (masukkan.Length % 3 == 0)
                {
                    for (int i = 0; i < masukkan.Length; i += 3)
                    {

                        string kata = masukkan.Substring(i, 3);
                        Console.WriteLine(kata);

                        if (kata.ToUpper() != "SOS")
                        {
                            n = 1;
                            jumlah += n;

                        }
                    }
                    Console.Write(jumlah);
                }
                else
                {
                    Console.WriteLine("Masukan sinyal lain");
                }

            }


        }
    }


        
    
