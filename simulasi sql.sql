create database DB_PTXA

create table biodata (
id int primary key identity (1,1),
first_name varchar (20) not null,
last_name varchar (30) not null,
dob date,
pob varchar(50) not null,
adress varchar (255) not null,
gender varchar (1) not null,)

insert into biodata (first_name, last_name, dob, pob, adress, gender) values
('soraya','rahayu','1990-12-22','Bali','Jl.Raya Kuta,Bali','P'),
('hanum','danuary','1990-01-02','Bandung','Jl.Berkah Ramadhan, Bandung','P'),
('melati','marcelia','1991-03-03','Jakarta','Jl.Mawar 3, Brebes','P'),
('farhan','Djokrowidodo','1989-10-11','Jakarta','Jl.Bahari Raya, Solo','L')

create table employee (
id int primary key identity (1,1),
biodata_id int not null,
nip varchar (5) not null,
statuss varchar (10) not null,
join_date date not null,
salary decimal (10,0) not null)

insert into employee (biodata_id,nip,statuss,join_date,salary) values
(1,'XA001','Permanen','2015-11-01 00:00:00:00',12000000),
(2,'XA002','Kontrak','2017-01-02 00:00:00:00',10000000),
(3,'XA003','Kontrak','2018-08-19 00:00:00:00',10000000)

create table contact_person (
id int primary key identity (1,1),
biodata_id int not null,
tipe varchar(5) not null,
contact varchar(100) not null)

insert into contact_person (biodata_id,tipe,contact) values
(1,'MAIL','soraya.rahayu@gmail.com'),
(1,'PHONE','085612345678'),
(2,'MAIL','harum.danuary@gmail.com'),
(2,'PHONE','081312345678'),
(2,'PHONE','087812345678'),
(3,'MAIL','melati.marcelia@gmail.com')

create table leave (
id int primary key identity(1,1),
tipe varchar(10) not null,
nama varchar(100) not null)

insert into leave (tipe, nama) values 
('Regular','Cuti Tahunan'),
('Khusus','Cuti Menikah'),
('Khusus','Cuti Haji & Umroh'),
('Khusus','Melahirkan')

create table employee_leave (
id int primary key identity (1,1),
employee_id int not null,
periode varchar(4) not null,
regular_kuota int not null)

insert into employee_leave (employee_id, periode, regular_kuota) values
(1,'2021',16),
(2,'2021',12),
(3,'2021',12)

create table leave_request (
id int primary key identity (1,1),
employee_id int not null,
leave_id int not null,
mulai_date date not null,
selesai_date date not null,
reason varchar(255) not null)

insert into leave_request ( employee_id, leave_id, mulai_date, selesai_date, reason) values
(1,1,'2021-10-10','2021-10-12','Liburan'),
(1,1,'2021-11-12','2021-11-15','Acara Keluarga'),
(2,2,'2021-05-05','2021-05-07','Menikah'),
(2,1,'2021-09-09','2021-09-13','Touring'),
(2,1,'2021-12-20','2021-12-23','Acara Keluarga')

select * from biodata
select * from contact_person
select * from employee
select * from employee_leave
select * from leave
select * from leave_request



--1. Menampilkan karyawan yang pertama kali masuk.
select b.first_name +' '+ b.last_name as name, d.join_date 
from biodata as b
join employee as d on b.id = d.id
where statuss = 'permanen'

--2. Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.
select d.nip, b.first_name +' '+ b.last_name as nama, g.mulai_date, 
datediff(day, g.mulai_date, g.selesai_date) as lama_cuti, g.reason
from biodata as b
join contact_person as c on b.id = c.id
join employee as d on c.id = d.id
join employee_leave as e on d.id = e.id
join leave as f on d.id = f.id
join leave_request as g on e.employee_id = g.employee_id
where '2021-12-22' between  g.mulai_date and g.selesai_date

 --3. Menampilkan daftar karyawan yang sudah mengajukan cuti lebih dari 2 kali. Tampilkan data berisi no_induk, nama, jumlah pengajuan .
 select d.nip, b.first_name + ' ' + b.last_name as nama, count(e.regular_kuota) as jumlah_cuti
 from biodata as b
join contact_person as c on b.id = c.id
join employee as d on c.id = d.id
join employee_leave as e on d.id = e.id
join leave as f on d.id = f.id
join leave_request as g on e.employee_id = g.employee_id
group by d.nip, b.first_name, b.last_name
having count(e.regular_kuota) > 2 

--4. Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah sesuaidengan quota cuti. Daftar berisi no_induk, nama, quota, cuti ygsudah di ambil dan sisa_cuti
	 select d.nip, b.first_name + ' ' + b.last_name as nama, e.regular_kuota,
	 case 
    when g.employee_id is not null 
    then sum(DATEDIFF(day,g.mulai_date,g.selesai_date)+1) 
    else 0
 end Cuti_Yang_Sudah_Diambil,
case 
    when g.employee_id is not null
    then (e.regular_kuota-sum(DATEDIFF(day,g.mulai_date,g.selesai_date)+1)) 
    else e.regular_kuota
    end Sisa_cuti
	 from biodata as b
	left join employee as d on b.id = d.id
left join employee_leave as e on d.biodata_id= e.employee_id
left join leave_request as g on e.employee_id = g.employee_id
where d.nip is not null
group by d.nip,b.first_name,b.last_name, e.regular_kuota, g.employee_id, e.employee_id

--5. Perusahaan akan meberikan bonus bagi karyawan yang sudah bekerja lebih dari 5 tahun sebanyak 1.5 kali gaji. 
-- Tampilan No induk, Fullname, Berapa lama bekerja, Bonus, Total Gaji(gaji + bonus)
select d.nip, b.first_name + ' ' + b.last_name as full_name,
datediff (year, d.join_date,'2021-12-22' ) as lama_kerja,
case
when datediff (year, d.join_date,'2021-12-22') > 5 then  (d.salary * 1.5)
else 0
end bonus,
case
when datediff (year,d.join_date,'2021-12-22') > 5 then ((d.salary * 1.5 )) + salary
else d.salary+0
end total_gaji
 from biodata as b
join contact_person as c on b.id = c.id
join employee as d on c.id = d.id
 join employee_leave as e on d.id = e.id
 join leave as f on d.id = f.id
 join leave_request as g on e.employee_id = g.employee_id
 where datediff (year,d.join_date,'2021-12-22') > 5
group by d.nip, first_name,last_name,d.join_date,d.salary

--6. Tampilkan nip, nama_lengkap, jika karyawan ada yg berulang tahun di hari ini 
--akan diberikan hadiah bonus sebanyak 5% dari gaji jika tidak ulang tahun maka bonus 0 dan total gaji . 
--Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji
select d.nip, b.first_name + ' ' + b.last_name as full_name,  b.dob,
datediff (year, b.dob,'2021/12/22') as usia,
case
when day(b.dob) = day ('2021/12/22')  and month(b.dob) = month('2021/12/22')
then d.salary * 0.05
else 0
end bonus,
case
when day(b.dob) = day ('2021/12/22')  and month(b.dob) = month('2021/12/22')
then (d.salary*0.05) + d.salary
else d.salary
end total_gaji
 from biodata as b
join employee as d on b.id = d.id
where d.nip = 'XA001'
group by d.nip, first_name,last_name,b.dob,d.salary
 
 --7. Tampilkan No Induk, nama, Tgl lahir , Usia. Urutkan biodata dari yg paling muda sampai yg tua
 select d.nip, b.first_name + ' ' + b.last_name as full_name, b.dob,
 datediff (year, b.dob,'2021/12/22') as usia 
 from biodata as b
 join employee as d on b.id = d.id
 order by usia asc

 --8 yg belom cuti
 select  b.first_name + ' ' + b.last_name as full_name
 from biodata as b
left join contact_person as c on b.id = c.id
left join employee as d on c.id = d.id
left join employee_leave as e on d.id = e.id
left join leave as f on d.id = f.id
left join leave_request as g on e.employee_id = g.employee_id
where g.employee_id is null and d.biodata_id is not null

--9. Tampikan Nama Lengkap, Jenis Cuti, Durasi Cuti, dan no telp yang sedang cuti
	select b.first_name + ' ' + b.last_name as full_name, f.nama,
	DATEDIFF(day,g.mulai_date,g.selesai_date) as durasi_cuti, 
	c.contact
	 from biodata as b
	 join employee as d on b.id = d.biodata_id
	join contact_person as c on d.biodata_id = c.biodata_id
	join employee_leave as e on d.biodata_id = e.employee_id
	join leave_request as g on e.employee_id = g.employee_id
	join leave as f on g.leave_id = f.id
	
	where g.mulai_date between  '2021-12-20' and '2021-12-23' and c.tipe = 'PHONE'
	group by b.first_name + ' ' + b.last_name, f.nama,g.mulai_date,g.selesai_date, c.contact


--10. Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
select b.first_name + ' ' + b.last_name as full_name
 from biodata as b
left join contact_person as c on b.id = c.id
left join employee as d on c.id = d.id
left join employee_leave as e on d.id = e.id
left join leave as f on d.id = f.id
left join leave_request as g on e.employee_id = g.employee_id
where d.biodata_id is null

--11. buatlah sebuah view yg menampilkan data nama lengkap, tgl lahir dan tmpat lahir , status, dan salary
create view vw_data as
select b.first_name + ' ' + b.last_name as full_name,b.dob,b.pob,d.statuss,d.salary
 from biodata as b
left join contact_person as c on b.id = c.id
left join employee as d on c.id = d.id
left join employee_leave as e on d.id = e.id
left join leave as f on d.id = f.id
left join leave_request as g on e.employee_id = g.employee_id
group by b.first_name,b.last_name,b.dob, b.pob, d.statuss,d.salary

select * from vw_data

--6. Tampilkan nip, nama_lengkap, jika karyawan ada yg berulang tahun di hari ini 
--akan diberikan hadiah bonus sebanyak 5% dari gaji jika tidak ulang tahun maka bonus 0 dan total gaji . 
--Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji
select d.nip, b.first_name +' '+ b.last_name as nama_lengkap, b.dob,
DATEDIFF(year, b.dob, '2021/12/22') as usia,
case
when day(b.dob) = day('2021/12/22') and month(b.dob) = month('2021/12/22')
then salary * 0.05
else 0
end bonus,
case
when day(b.dob) = day('2021/12/22') and month(b.dob) = month('2021/12/22')
then (salary * 0.05) + d.salary
else d.salary
end total_gaji
from biodata as b
join employee as d on b.id = d.id



















