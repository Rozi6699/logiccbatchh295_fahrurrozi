﻿using System;
using System.Linq;

namespace LogicDay5
{
    internal class Program
    {
        static void Main(String[] args)
        {

            {
                Console.Write("Pilih Soal Kamu = ");
                string pilih = Console.ReadLine();

                switch (pilih)
                {
                    case "Soal1":
                        Soal1();
                        break;
                    case "Soal2":
                        Soal2();
                        break;
                    case "Soal3":
                        Soal3();
                        break;
                    case "Soal4":
                        Soal4();
                        break;
                    case "Soal5":
                        Soal5();
                        break;
                    case "Soal6":
                        Soal6();
                        break;
                }
            }
        }
        static void Soal1()
        {
            Console.Write("Masukkan Jam = ");
            String jam = Console.ReadLine();
            int waktu = int.Parse(jam.Substring(0, 2));
            string itunganjam = jam.Substring(8, 2);
            if (itunganjam == "PM")
            {

                if (waktu < 12)
                {
                    waktu += 12;

                }
            }
            else if (itunganjam == "AM")
            {
                if (waktu == 12)
                {
                    waktu -= 12;
                    Console.Write("0");
                }

            }
            Console.Write(waktu + jam.Substring(2, 6));
        }
        static void Soal2()
        {
            Console.Write("Total menu : ");
            int menu = int.Parse(Console.ReadLine());
            Console.Write("Index makanan alergi : ");
            int alergi = int.Parse(Console.ReadLine());
            Console.Write("Harga menu : ");
            string[] hargaMenu = Console.ReadLine().Split(",");
            Console.Write("Jumlah uang elsa : ");
            int uang = int.Parse(Console.ReadLine());

            int[] harga = Array.ConvertAll(hargaMenu, int.Parse);

            int sum = 0;
            for (int i = 0; i < harga.Length; i++)
            {
                sum += harga[i];

            }
            int jumlah = sum - harga[alergi];
            int total = jumlah / 2;
            int sisaUang = uang - total;

            Console.WriteLine();
            Console.WriteLine($"Elsa harus membayar = Rp.{total}");

            if (sisaUang == 0)
            {
                Console.WriteLine("Uang elsa pas");
            }
            else
            {
                Console.WriteLine($"Sisa uang elsa Rp.{sisaUang}");
            }

        }
        static void Soal3()
        {
            int[,] angka = new int[,]
            {{11, 2, 4},
             {4, 5, 6},
             {10, 8, -12}
            };
            int temp = 0;
            int temp1 = 0;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == j)
                    {
                        temp += angka[i, j];
                    }
                    if (i == i && j == 3 - i - 1)
                    {
                        temp1 += angka[i, j];
                    }

                }
            }
            Console.WriteLine($"Perbedaan Diagonal 1 dan 2 {temp - temp1}");


        }
        static void Soal4()
        {
            int jumlah = 0;
            Console.Write("Lilin = ");
            string[] lilin = Console.ReadLine().Split(" ");
            int[] ConvertLilin = Array.ConvertAll(lilin, int.Parse);

            int maxLilin = ConvertLilin.Max();
            for (int i = 0; i < ConvertLilin.Length; i++)
            {
                if (maxLilin == ConvertLilin[i])
                {
                    jumlah++;
                }
            }
            Console.WriteLine($"Jumlah Lilin yang Ditiup = {jumlah}");
        }
        static void Soal5()
        {
            string hasil, input;
            int rotate;
            Console.Write("Masukkan input = ");
            input = Console.ReadLine();
            String[] arrayinput = input.Split(",");
            Console.Write("Masukkan rotate = ");
            rotate = int.Parse(Console.ReadLine());

            hasil = String.Empty;
            for (int i = 1; i <= rotate; i++)
            {
                for (int j = 0; j < arrayinput.Length - 1; j++)
                {
                    hasil = arrayinput[i];
                    arrayinput[i] = arrayinput[j + 1];
                    arrayinput[j + 1] = hasil;
                }
                Console.WriteLine("Hasil Rotate = ");
                foreach (string jumlah in arrayinput)
                {
                    Console.Write(jumlah);
                }

                Console.WriteLine();
            }
        }
        static void Soal6()
        {
            string hasil, input;
            int rotate;
            Console.Write("Masukkan input = ");
            input = Console.ReadLine();
            String[] arrayinput = input.Split(",");
            Console.Write("Masukkan rotate = ");
            rotate = int.Parse(Console.ReadLine());

            hasil = String.Empty;
            for (int i = 1; i <= rotate; i++)
            {
                for (int j = 0; j < arrayinput.Length - 1; j++)
                {
                    hasil = arrayinput[0];
                    arrayinput[0] = arrayinput[j + 1];
                    arrayinput[j + 1] = hasil;
                }
                Console.WriteLine("Hasil Rotate = ");
                foreach (string jumlah in arrayinput)
                {
                    Console.Write(jumlah);
                }

                Console.WriteLine();
            }
        }
    }
}

    
