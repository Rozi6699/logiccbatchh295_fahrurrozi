create database DB_Sales

create table salesperson(
ID int primary key,
Nama varchar (50),
BOD date,
Salary decimal(18,2))

create table orders(
ID int primary key,
Order_dare date,
Cust_id int,
Salesperson_id int,
Amount decimal (18,2))

insert into salesperson(ID, Nama, BOD, Salary) values
(1,'Abe','9-11-1988',140000),
(2,'Bob','9-11-1978',44000),
(3,'Chris','9-11-1983',40000),
(4,'Dan','9-11-1980',52000),
(5,'Ken','9-11-1977',115000),
(6,'Joe','9-11-1990',38000)

insert into orders(ID, Order_dare, Cust_id, Salesperson_id, Amount) values
(1,'8-02-2020',4,2,540),
(2,'1-22-2021',4,5,1800),
(3,'7-14-2019',9,1,460),
(4,'1-29-2018',7,2,2400),
(5,'2-03-2021',6,4,600),
(6,'3-02-2020',6,4,720),
(7,'5-06-2021',9,4,150)

select * from salesperson
select * from orders

--a. informasi nama sales yang memiliki order lebih dari 1.
select s.nama, s.id, count(d.Salesperson_ID) as jumlah_order
from salesperson as s
join orders as d on s.id = d.Salesperson_ID
group by s.nama ,s.id
having count(d.Salesperson_ID) >1

--b. Informasi nama sales yang total amount ordernya di atas 1000.
select s.nama, s.id, SUM(amount) as jumlah_amount
from salesperson as s
join orders as d on s.id = d.Salesperson_id
group by s.nama, s.id
having SUM (amount) > 1000
 
--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending).
select s.nama, datediff( year, s.bod, getdate()) as umur 
from salesperson as s
join orders as d on s.id = d.Salesperson_id
where year(d.order_dare) >= 2020
group by s.nama, datediff(year, s.bod,getdate())
order by umur asc

--d. Carilah rata-rata total amount masing-masig sales urutkan dari hasil yg paling besar
select s.nama, AVG (amount) as jumlah_amount
from salesperson as s
left join orders as d on s.id = d.Salesperson_id
group by s.nama
order by jumlah_amount desc


--e.perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary
select s.id, s.nama, count(d.salesperson_id) as total_order, sum(d.amount) as total_amount, 
case 
	when SUM(d.Amount) > 1000 and count(d.salesperson_id) > 2 then s.salary * 0.3

end bonus
from salesperson as s
join orders as d on s.id = Salesperson_id
group by s.id, s.nama, s.salary
having count(d.Salesperson_id) > 2 

--f. Tampilkan data sales yang belum memiliki orderan sama sekali
select s.nama
from salesperson as s
left join orders as d on s.id = d.Salesperson_id
where d.salesperson_id is null

--g. Gaji sales akan dipotong jika tidak memiliki orderan,  gaji akan di potong sebanyak 2%
select s.nama, s.salary,
case 
	when count(d.salesperson_id) = 0 then s.Salary - (s.Salary * 0.02)
	else s.salary
end total_gaji
from salesperson as s
left join orders as d on s.id = d.Salesperson_id
group by s.nama, s.id, s.salary, d.Salesperson_id


