create database DB_Univ_XA

create table tbl_mahasiswa(
nim int primary key,
nama varchar (100),
jenis_kelamin varchar (100),
alamat varchar (100))

insert into tbl_mahasiswa (nim, nama, jenis_kelamin, alamat) values
(101,'Arif','L','Jl. Kenangan'),
(102,'Budi','L','Jl. Jombang'),
(103,'Wati','P','Jl. Surabaya'),
(104,'Ika','P','Jl. Jombang'),
(105,'Tono','L','Jl. Jakarta'),
(106,'Iwan','L','Jl. Bandung'),
(107,'Sari','P','Jl. Malang')

create table tbl_matakuliah (
ID int primary key identity (1,1),
kode_mk varchar (50),
nama_mk varchar (100),
sks int,
semester int)

insert into tbl_matakuliah (kode_mk, nama_mk, sks, semester) values
('PTI447','Praktikum Basis Data',1,3),
('TIK342','Praktikum Basis Data',1,3),
('PTI333','Basis Data Terdistribusi',3,5),
('TIK123','Jaringan Komputer',2,5),
('TIK333','Sistem Operasi',3,5),
('PTI123','Grafika Multimedia',3,5),
('PTI777','Sistem Informasi',2,3)

create table ambil_mk (
Nim int,
kode_mk varchar (50))
drop table ambil_mk

insert into ambil_mk (Nim, kode_mk) values
(101,'PTI447'),
(103,'TIK333'),
(104,'PTI333'),
(104,'PTI777'),
(111,'PTI123'),
(123,'PTI999')

--1. Tampilkan nama mahasiswa dan matakuliah yang diambil
select siswa.nama, kuliah.nama_mk
from tbl_mahasiswa as siswa
join ambil_mk as mk on siswa.nim = mk.Nim
join tbl_matakuliah as kuliah on mk.kode_mk = kuliah.kode_mk

--2. Tampilkan data mahasiswa yang tidak mengambil matakuliah
select siswa.nim, siswa.nama, siswa.jenis_kelamin, siswa.alamat 
from tbl_mahasiswa as siswa
left join ambil_mk as mk on siswa.nim = mk.Nim
left join tbl_matakuliah as kuliah on mk.kode_mk = kuliah.kode_mk
where kuliah.kode_mk is null

--3. Kelompokan data mahasiswa yang tidak mengambil matakuliah berdasarkan jenis kelaminnya, kemudian hitung banyaknya.
select count(siswa.nim) as jml, jenis_kelamin
from tbl_mahasiswa as siswa
left join ambil_mk as mk on siswa.nim = mk.Nim
left join tbl_matakuliah as kuliah on mk.kode_mk = kuliah.kode_mk
where mk.nim is null
group by jenis_kelamin

--4. Dapatkan nim dan nama mahasiswa yang mengambil matakuliah beserta kode_mk dan nama_mk yang diambilnya
select siswa.nim, siswa.nama, mk.kode_mk, kuliah.nama_mk
from tbl_mahasiswa as siswa
left join ambil_mk as mk on siswa.nim = mk.Nim
left join tbl_matakuliah as kuliah on mk.kode_mk = kuliah.kode_mk
where kuliah.kode_mk is not null

--5. Dapatkan nim, nama, dan total sks yang diambil oleh mahasiswa, Dimana total sksnya lebih dari 4 dan kurang dari 10.
select siswa.nim, siswa.nama, SUM(kuliah.sks) as jml_sks
from tbl_mahasiswa as siswa
left join ambil_mk as mk on siswa.nim = mk.Nim
left join tbl_matakuliah as kuliah on mk.kode_mk = kuliah.kode_mk
group by siswa.nim, siswa.nama
having SUM(kuliah.sks) > 4

--6. Dapatkan data matakuliah yang tidak diambil oleh mahasiswa terdaftar (mahasiswa yang terdaftar adalah mahasiswa yang tercatat di tabel mahasiswa).
select kuliah.nama_mk
from tbl_mahasiswa as siswa
join ambil_mk as mk on siswa.nim = mk.Nim
right join tbl_matakuliah as kuliah on mk.kode_mk = kuliah.kode_mk
where siswa.nama is null











