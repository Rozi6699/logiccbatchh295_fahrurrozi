﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace LogicDay7
{
    internal class Program
    {
        static void Main(String[] args)
        {

            {
                Console.Write("Pilih Soal Kamu = ");
                string pilih = Console.ReadLine();

                switch (pilih)
                {
                    case "Date":
                        Date();
                        break;
                    case "Soal1":
                        Soal1();
                        break;
                    case "Soal2":
                        Soal2();
                        break;
                    case "Soal3":
                        Soal3();
                        break;
                    case "Soal4":
                        Soal4();
                        break;
                    case "Soal5":
                        Soal5();
                        break;
                    case "Soal6":
                        Soal6();
                        break;
                    case "Soal7":
                        Soal7();
                        break;
                   
                }
            }
        }
        static void Date()
        {
            DateTime date = new DateTime();
            Console.WriteLine(date);

            DateTime date1 = new DateTime(2022, 7, 8, 10, 30, 00);
            Console.WriteLine(date1);

            DateTime dateNow = DateTime.Now;
            Console.WriteLine(dateNow);

            Console.Write("Input Tanggal (mm/dd/yyyy) = ");
            string input = Console.ReadLine();
            DateTime dateInput = DateTime.Parse(input);
            Console.WriteLine(dateInput.ToString("dd MMM yyyy"));

            int year = dateNow.Year;
            int menit = dateNow.Minute;
            var hari = dateNow.DayOfWeek;
            Console.WriteLine(year);
            Console.WriteLine(menit);
            Console.WriteLine(hari);

            TimeSpan interval = dateInput - dateNow;

            Console.WriteLine(interval.Days);

        }
        static void Soal1()
        {
            Console.Write("Masukkan Waktu Awal = ");
            string awal = Console.ReadLine();
            DateTime masuk = DateTime.Parse(awal);

            Console.Write("Masukkan Waktu keluar = ");
            string akhir = Console.ReadLine();
            DateTime keluar = DateTime.Parse(akhir);


            TimeSpan time = keluar - masuk;
            Console.WriteLine($"Anda Telah Parkir Selama {time} Jam");

            int jam = time.Hours;
            int harga = 3000;

            int total = jam * harga;
            Console.WriteLine($"Total Yang Harus Dibayar Adalah {total}");

        }
        static void Soal2()
        {
            Console.Write("Masukkan Tanggal Peminjaman Buku = ");
            string tanggal = Console.ReadLine();
            DateTime meminjam = DateTime.Parse(tanggal); //convert String ke int

            Console.Write("Masukkan Tanggal Pengembalian Buku = "); //masukkan input tanggal pengembalian
            string balik = Console.ReadLine();
            DateTime balikk = DateTime.Parse(balik);

            int totalHarga = 0;

            if (balikk > meminjam)
            {
                TimeSpan jarak = balikk - meminjam.AddDays(3); //menghitung interval
                int Jarak = (int) jarak.TotalDays; //mengambil jumlah hari dari pengurangan
                totalHarga = Jarak * 500;
                Console.WriteLine($"{totalHarga}");
            }
        }
        static void Soal3()
        {
            Console.Write("Input lokasi ke : ");
            int lokasi = int.Parse(Console.ReadLine());

            double customer1 = 2;
            double customer2 = 0.5;
            double customer3 = 1.5;
            double customer4 = 0.3;

            double hasil = 0;

            if (lokasi == 1)
            {
                hasil = customer1;
                Console.WriteLine($"Jarak Tempuh = {hasil} KM");
            }
            else if (lokasi == 2)
            {
                hasil = customer1 + customer2;
                Console.WriteLine($"Jarak Tempuh = {customer1} KM +{customer2 * 1000} M = {hasil} KM");
            }
            else if (lokasi == 3)
            {
                hasil = customer1 + customer2 + customer3;
                Console.WriteLine($"Jarak Tempuh = {customer1} KM + {customer2 * 1000} M + {customer3} KM = {hasil} KM");
            }
            else if (lokasi == 4)
            {
                hasil = customer1 + customer2 + customer3 + customer4;
                Console.WriteLine($"Jarak Tempuh = {customer1} KM + {customer2 * 1000} KM + {customer3} KM + {customer4 * 1000} M = {hasil} KM");
            }
            double liter = hasil / 2.5;

            double jmlLiter = Math.Ceiling(liter);
            Console.WriteLine($"Anda menghabiskan bensin sebanyak {jmlLiter} liter");
        }
    
        static void Soal4()
        {
            int totalHarga = 0;
           
            Console.Write("Masukkan Waktu Sewa = ");
            int jam = int.Parse(Console.ReadLine());
            

            Console.Write("Masukkan Waktu Awal Bermain = ");
            string jam1 = Console.ReadLine();
            DateTime bermain = DateTime.Parse(jam1);

            int waktu = bermain.Hour + jam;

            totalHarga = jam * 3500;

            Console.Write("Masukkan Tambahan Biling = ");
            int biling = int.Parse(Console.ReadLine());

            int jamAkhir = waktu + biling;
            int totalBiaya = totalHarga + (biling * 3500);

            TimeSpan jumlahTambah = bermain.AddHours(jam + biling) - bermain;
            int hargaTambah = jumlahTambah.Hours * 3500;
            Console.WriteLine($"Akan selesai pada jam {bermain.AddHours(jam + biling)} Total Harga {hargaTambah}");


        }
        static void Soal5()
        {
            Console.Write("Input Antar Customer: ");
            double inputAntarCustomer = double.Parse(Console.ReadLine());
            double tokoCS1 = 2000, cs1cs2 = 500, cs2cs3 = 1500, cs3cs4 = 300;
            double satuLiterBensin = 2500;
            double jarakTempuh = 0;
            double pemakaianBensin = 0;
            if (inputAntarCustomer == 1)
            {
                jarakTempuh = tokoCS1;
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 2)
            {
                jarakTempuh = (tokoCS1 + cs1cs2);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 3)
            {
                jarakTempuh = (tokoCS1 + cs1cs2 + cs2cs3);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }            if (inputAntarCustomer == 4)
            {
                jarakTempuh = (tokoCS1 + cs1cs2 + cs2cs3 + cs3cs4);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            Console.WriteLine($"Jarak Tempuh = {jarakTempuh / 1000} KM");
            Console.WriteLine($"Bensin = {Math.Round(pemakaianBensin)}");
        }
    
        static void Soal6()
        {
            Console.Write("Masukan x! = ");
            int x = int.Parse(Console.ReadLine());

            int fak = 1;
            Console.Write($"{x}!=");
            for (int i = x; i >= 1; i--)
            {
                fak *= i;
                Console.Write($"{i}");
                if (i > 1)
                {
                    Console.Write($"x");
                }
                else
                    Console.Write("=");

            }
            Console.Write(fak);
        }
        static void Soal7()
        {

            int ulang1 = 0;
            Console.Write("Masukan Point = "); //poin taruhannya untuk menebak
            int point = int.Parse(Console.ReadLine());
            do
            {
                Console.Write("Masang Taruhan = "); //beeting untuk masang harga tebakan awal
                int masangTaruhan = int.Parse(Console.ReadLine());
                Console.Write("Masukan Jawaban Tebakan Anda U/D = "); // jawaban taruhan ketika yakin
                string answerSure = Console.ReadLine().ToUpper();
                Random randomAngka = new Random(); // Fungsi Random angka di komputer
                int numbers = randomAngka.Next(0, 9); // angka random mulai dari 0-9 yang telah disediakan/ sudah di deteksi
                bool ulangiPermainan = true; // kondisi spesifik data untuk pengulangan permainan
                string password = ""; /// buat memberikan jawaban yang akan di hasilnya
                if (point > 0)
                {
                    //ulangiPermainan = false;
                    if (masangTaruhan <= point) //point ke update
                    {
                        if (numbers > 0)
                        {
                            password = "U";
                        }
                        if (numbers < 0)
                        {
                            password = "D";
                        }
                        if (answerSure == password)
                        {
                            point += masangTaruhan;
                            Console.WriteLine("You Win");
                        }
                        if (answerSure != password)
                        {
                            point -= masangTaruhan;
                            Console.WriteLine("You Lose");
                        }
                        if (numbers == 5)
                        {
                            point = 0;
                            Console.WriteLine("SERI");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Anda tidak bisa masang taruhan lebih dari = {point}");
                    }

                }
                if (point <= 0)
                {
                    point = 0;
                    Console.WriteLine("Game Over");
                }
                Console.WriteLine($"Point Saat ini = {point}");
                ulang1 = point;// ini untuk perulangan WHILE
            }
            while (ulang1 > 0);
        }
        static void Soal8()
        {

        }
    }
}